import { GraphQLRequestContext, GraphQLResponse } from "apollo-server-types";
import { GraphQLDataSource } from "@apollo/gateway";
import HydraRequest, { RequestMethod } from "./hydraHttpRequest";
import { Headers } from "apollo-server-env";

export default class HydraApolloDataSource<TContext extends Record<string, any> = Record<string, any>> implements GraphQLDataSource<TContext> {
    hydraHttpRequest: HydraRequest;
    conf: { name: string; path?: string };
    constructor(conf: { name: string; path?: string }, hydraHttpRequest: HydraRequest) {
        this.hydraHttpRequest = hydraHttpRequest;
        this.conf = conf;
    }

    async process({ request }: Pick<GraphQLRequestContext<TContext>, "request" | "context">): Promise<GraphQLResponse> {
        const headers = (request.http && request.http.headers) || new Headers();
        headers.set("Content-Type", "application/json");
        request.http = {
            method: "POST",
            url: this.conf.name,
            headers,
        };
        console.log({request, http: {
            method: "POST",
            url: this.conf.name,
            headers,
        }});


        const headerEntries = request.http?.headers.entries() ?? new Headers().entries();
        const headerObject: { [key: string]: string } = {};
        let iteratorResult = headerEntries.next();
        while (!iteratorResult.done) {
            if (iteratorResult.value != null) headerObject[iteratorResult.value[0]] = iteratorResult.value[1];
            iteratorResult = headerEntries.next();
        }

        const httpMethod = request.http?.method.toUpperCase();
        // console.log(Object.keys(HttpRequestMethod))

        if (!Object.keys(RequestMethod).includes(httpMethod)) throw new Error(`Failed to parse HTTP method in HydraApolloDataSource: ${httpMethod}`);

        const { http, ...requestWithoutHttp } = request;
        const result = await this.hydraHttpRequest.send(
            this.conf.name,
            httpMethod as RequestMethod,
            this.conf.path ?? "/gql",
            requestWithoutHttp as string | { [key in string | number]?: unknown },
            headerObject,
        );
        return {
            ...(result.body as { [key: string]: unknown }),
        };
    }
}
